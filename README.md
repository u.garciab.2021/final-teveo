

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Uriel García Barbulo
* Titulación: Ingeniería en Sistemas de Telecomunicaicón
* Cuenta en laboratorios: ugarciab
* Cuenta URJC: u.garciab.2021
* Video básico (url): https://www.youtube.com/watch?v=vuSG6AkTb-4
* Video parte opcional (url): https://www.youtube.com/watch?v=izM17p-mZ8o
* Despliegue (url): https://yurich.pythonanywhere.com/
* Contraseñas: 
* Cuenta Admin Site: superuser/1234

## Resumen parte obligatoria

He diseñado una aplicación web llamada TeVeO que consta varios recursos. Primero nos encontramos una página principal con una lista de comentarios de usuarios de cámaras. Desde estos comentarios podremos acceder a las cámaras. Tiene un apartado de cámaras en el que se pueden descargar dos listados de cámaras y además se muestran las cámaras descargadas. Podremos acceder a las páginas de cada cámara estáticas o dinámicas. Las estáticas nos permiten acceder al recurso de comentar para poner un comentario. Las dinámicas permiten comentar desde las mismas y se recarga la imagen y los comentarios cada 30 segundos.

Además, tenemos la opción de cambiar el nombre de usuario, el tamaño de letra y la fuente de letra en el apartado de configuración, también teniendo la opción de copiar la url del usuario para usar ese mismo usuario en otro navegador. Tenemos un poco de inofrmación en el apartado de ayuda. Tenemos el apartado de admin para ver la base de datos. Por último, tenemos la opción de en cada web de camara agregar a la url ".json" para obtener el json de la información de la cámara.

## Lista partes opcionales

* Nombre parte: favicon, se ha añadido un favicon a la web (una T).
* Nombre parte: cerrar sesión, se da la posibilidad al usuario de marcar una casilla de cerrar sesión.
* Nombre parte: likes, se permite a cada usuario dar 1 like por cámara.
