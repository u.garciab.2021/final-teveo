from django.test import TestCase, Client
from .models import Camara, Usuario, Comentario
from django.utils import timezone
from django.urls import reverse

class CamaraModelTests(TestCase):

    def test_create_camara(self):
        camara = Camara.objects.create(
            id_camara='LIST - 2',
            latitud='5.235235235235',
            longitud='5.235235235235',
            localizacion='Peru',
            num_comentarios=0,
            enlace='https://informo.madrid.es/cameras/Camara06305.jpg'
        )
        self.assertTrue(isinstance(camara, Camara))
        self.assertEqual(camara.id_camara, 'LIST - 2')

class UsuarioModelTests(TestCase):

    def test_create_usuario(self):
        usuario = Usuario.objects.create(
            nombre='pedro',
            id_usuario='pedro',
            fuente_usuario='monospace',
            size_fuente=18
        )
        self.assertTrue(isinstance(usuario, Usuario))
        self.assertEqual(usuario.nombre, 'pedro')

class ComentarioModelTests(TestCase):

    def test_create_comentario(self):
        usuario = Usuario.objects.create(
            nombre='pedro',
            id_usuario='pedro',
            fuente_usuario='monospace',
            size_fuente=18
        )
        camara = Camara.objects.create(
            id_camara='LIST - 2',
            latitud='5.235235235235',
            longitud='5.235235235235',
            localizacion='Peru',
            num_comentarios=0,
            enlace='https://informo.madrid.es/cameras/Camara06305.jpg'
        )
        comentario = Comentario.objects.create(
            fecha=timezone.now(),
            usuario=usuario,
            texto='holaaaa',
            id_camara=camara
        )
        self.assertTrue(isinstance(comentario, Comentario))
        self.assertEqual(comentario.texto, 'holaaaa')
        self.assertEqual(comentario.usuario, usuario)
        self.assertEqual(comentario.id_camara, camara)


class EndToEndTest(TestCase):

    def setUp(self):
        pass

    def test_index_page_loads_correctly(self):
        client = Client()

        response = client.get(reverse('index'))

        # 200 ok
        self.assertEqual(response.status_code, 200)

        # al menos una camara renderizada
        self.assertGreaterEqual(len(response.context['camaras']), 1)

        # la información de la camara se muestra correctamente
        camaras = response.context['camaras']
        for camara_info in camaras:
            # cada cámara tiene información valida
            camara_id = camara_info.get('id_camara')
            camara_obj = Camara.objects.get(id_camara=camara_id)
            self.assertIsNotNone(camara_obj)
            self.assertEqual(camara_info.get('enlace'), camara_obj.enlace)