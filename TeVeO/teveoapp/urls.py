from django.urls import path
from . import views
urlpatterns = [
      path('', views.index, name='index'),
      path('camaras/', views.camaras, name='camaras'),
      path('configuracion/', views.configuracion, name='configuracion'),
      path('ayuda/', views.ayuda, name='ayuda'),
      path('admin/', views.admin, name='admin'),
      path('comentarios/', views.comentarios, name='comentarios'),
      path('<str:identificador_cam>.json/', views.camara_json, name='json de camara'),
      path('<str:id_camara>-dyn/', views.camara_dyn, name='camara dinamica'),
      path('<str:id_camara>/imagen/', views.camara_imagen, name='camara_imagen'),
      path('<str:identificador_cam>/', views.camara, name='camara'),
]


