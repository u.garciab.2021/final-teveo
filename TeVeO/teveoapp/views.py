from django.http import HttpResponse
from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.utils import timezone
from xml.sax import parseString
import base64
import string
import urllib.request
import random
from .models import Usuario, Comentario, Camara
from django.conf import settings
from pathlib import Path
import os
from django.conf import settings
from .parse import Handler1 , Handler2
from django.shortcuts import render, get_object_or_404



def gestion_usuarios(request):
    send_cookie = False
    usuario = ''
    if request.COOKIES.get('usuario'):
        id_usuario = request.COOKIES.get('usuario')
        try:
            usuario = Usuario.objects.get(id_usuario=id_usuario)
        except:
            usuario = ''
    else:
        usuario = ''
    
    if usuario == '': # creamos un nuevo usuario
        id_usuario = ''.join(random.choices(string.ascii_lowercase + string.digits, k=30))
        usuario = Usuario(id_usuario = id_usuario)
        usuario.save()
        send_cookie = True

    return usuario, send_cookie

def index(request):
    usuario, send_cookie = gestion_usuarios(request)
    comentarios = Comentario.objects.all()
    comentarios = comentarios.order_by('-fecha')
    total_camaras = Camara.objects.count()
    total_comentarios = Comentario.objects.count()
    camaras = Camara.objects.order_by('-num_comentarios')

    comentarios_con_imagenes = []
    for comentario in comentarios:
        imagen_base64 = base64.b64encode(comentario.imagen).decode('utf-8')
        comentarios_con_imagenes.append({
            'fecha': comentario.fecha,
            'usuario': comentario.usuario,
            'imagen_base64': imagen_base64,
            'texto': comentario.texto,
            'id_camara': comentario.id_camara,
    })

    response = render(request, 'index.html', {'camaras': camaras, 'comentarios': comentarios_con_imagenes, 'total_camaras': total_camaras, 'total_comentarios': total_comentarios, 'usuario': usuario})
    if send_cookie:
        response.set_cookie('usuario', usuario.id_usuario, expires=timezone.now() + timezone.timedelta(days=365))

    return response

def camaras(request):
    usuario, send_cookie = gestion_usuarios(request)
    if request.method == 'POST':
        listado = request.POST['listado']
        if listado == '1':
            todo = ''
            file_path = Path('./estaticos/listado1.xml')
            with open(file_path, 'r', encoding='utf-8') as file:
                for line in file:
                    todo += line
            parseString(todo, Handler1())
        elif listado == '2':
            todo = ''
            file_path = Path('./estaticos/listado2.xml')
            with open(file_path, 'r', encoding='utf-8') as file:
                for line in file:
                    todo += line
            parseString(todo, Handler2())

    camaras = Camara.objects.order_by('-num_comentarios')
    total_camaras = Camara.objects.count()
    total_comentarios = Comentario.objects.count()

    if total_camaras > 0:
        camara_ramdom = random.choice(list(camaras))
        camara_ramdom_enlace = camara_ramdom.enlace
    else:
        camara_ramdom = None
        camara_ramdom_enlace = 'https://cdn.discordapp.com/attachments/846469152491307028/1255510176597934118/tranparente_1.png?ex=667d6478&is=667c12f8&hm=5671c48539cad6fb7efcc934358ed42fd094732f4aa2a3a70df2a13a5669c587&'
    response = render(request, 'camaras.html', context={'camaras': camaras, 'camara_ramdom_enlace': camara_ramdom_enlace, 'total_camaras': total_camaras, 'total_comentarios': total_comentarios, 'usuario': usuario})

    if send_cookie:
        response.set_cookie('usuario', usuario.id_usuario, expires=timezone.now() + timezone.timedelta(days=365))


    return response

def configuracion(request):
    usuario, send_cookie = gestion_usuarios(request)
    total_camaras = Camara.objects.count()
    total_comentarios = Comentario.objects.count()

    autenticacion = "/teveoapp/configuracion/?id=" + usuario.id_usuario

    if request.method == 'GET':
        if request.GET.get('id'):
            key = request.GET.get('id')
            try:
                configuracion = Usuario.objects.get(id_usuario=key)
                usuario.fuente_usuario = configuracion.fuente_usuario
                usuario.size_fuente = configuracion.size_fuente
                usuario.nombre = configuracion.nombre
                usuario.save()
            except:
                print('No existe el usuario')


    if request.method == 'POST':
        nombre = request.POST['nombre']
        fuente = request.POST['fuente']
        tamano = request.POST['tamano']
        try:
            sesion = request.POST['cerrar_sesion']
        except:
            sesion = ''
        usuario.fuente_usuario = fuente
        usuario.size_fuente = tamano
        usuario.nombre = nombre
        usuario.save()

        if sesion == 'Cerrar':
            usuario = ''
            response = render(request, 'configuracion.html', {'total_camaras': total_camaras, 'total_comentarios': total_comentarios, 'usuario': usuario, 'url_autenticacion': autenticacion})
            response.delete_cookie('usuario')
            return response
    response = render(request, 'configuracion.html', {'total_camaras': total_camaras, 'total_comentarios': total_comentarios, 'usuario': usuario, 'url_autenticacion': autenticacion})

    if send_cookie:
        response.set_cookie('usuario', usuario.id_usuario, expires=timezone.now() + timezone.timedelta(days=365))
    return response

def ayuda(request): 
    usuario, send_cookie = gestion_usuarios(request)
    total_camaras = Camara.objects.count()
    total_comentarios = Comentario.objects.count()
    response = render(request, 'ayuda.html',  context={'total_camaras': total_camaras, 'total_comentarios': total_comentarios, 'usuario': usuario})
    if send_cookie:
        response.set_cookie('usuario', usuario.id_usuario, expires=timezone.now() + timezone.timedelta(days=365))
    return response

def admin(request):
    response = render(request, 'admin.html')
    return response

def camara(request, identificador_cam):
    usuario, send_cookie = gestion_usuarios(request)
    total_camaras = Camara.objects.count()
    total_comentarios = Comentario.objects.count()
    camara = Camara.objects.get(id_camara=identificador_cam)
    if request.method == 'POST':
        if usuario.id_usuario not in camara.usuarios_who_liked:
            camara.numero_likes += 1
            camara.usuarios_who_liked += usuario.id_usuario + ','
            camara.save()

    # sacamos los comentarios de la camara
    comentarios = Comentario.objects.filter(id_camara=camara)

    response = render(request, 'camara.html', context={'comentarios': comentarios, 'camara': camara, 'total_camaras': total_camaras, 'total_comentarios': total_comentarios, 'usuario': usuario})
    if send_cookie:
        response.set_cookie('usuario', usuario.id_usuario, expires=timezone.now() + timezone.timedelta(days=365))
    return response

def descargar_imagen(url):
    imagen = urllib.request.urlopen(url).read()
    return imagen

def comentarios(request):
    now = timezone.now()
    identificador = request.GET.get('identificador')
    camara = Camara.objects.get(id_camara=identificador)
    usuario, send_cookie = gestion_usuarios(request)
    total_camaras = Camara.objects.count()
    total_comentarios = Comentario.objects.count()
    if request.method == 'POST':
        comentario = request.POST['comentario']

        imagen = descargar_imagen(camara.enlace)
        comentario_usuario = Comentario(fecha=now, usuario=usuario, imagen=imagen, texto=comentario, id_camara=camara)
        if not Comentario.objects.filter(texto=comentario).exists():
            comentario_usuario.save()
        camara.num_comentarios += 1
        camara.save()

    comentarios = Comentario.objects.all()

    response = render(request, 'comentarios.html', context={'comentarios': comentarios, 'current_datetime': now, 'camara': camara, 'total_camaras': total_camaras, 'total_comentarios': total_comentarios, 'usuario': usuario})
    if send_cookie:
        response.set_cookie('usuario', usuario.id_usuario, expires=timezone.now() + timezone.timedelta(days=365))
    return response

def camara_json(request, identificador_cam):
    camara = Camara.objects.get(id_camara=identificador_cam)
    try:
        return JsonResponse({'camara_id': camara.id_camara, 'localizacion_camara': camara.localizacion, 'numero_comentarios': camara.num_comentarios, 'camara_url': camara.enlace, 'latitud': camara.latitud, 'longitud': camara.longitud})
    except ObjectDoesNotExist:
        return JsonResponse({'error': 'Algún parametro no está en la cámara'}, status=404)
    
def camara_dyn(request, id_camara):
    usuario, send_cookie = gestion_usuarios(request)
    camara = Camara.objects.get(id_camara=id_camara)

    total_camaras = Camara.objects.count()
    total_comentarios = Comentario.objects.count()
    
    camara = Camara.objects.get(id_camara=id_camara)

    comentarios = Comentario.objects.filter(id_camara=camara)

    

    if request.method == 'POST':
        comentario = request.POST['comentario']
        now = timezone.now()
        imagen = descargar_imagen(camara.enlace)
        comentario_usuario = Comentario(fecha=now, usuario=usuario, imagen=imagen, texto=comentario, id_camara=camara)
        if not Comentario.objects.filter(texto=comentario).exists():
            comentario_usuario.save()
        camara.num_comentarios += 1
        camara.save()

    response = render(request, 'camaradinamica.html', context={'comentarios': comentarios, 'camara': camara, 'total_camaras': total_camaras, 'total_comentarios': total_comentarios, 'usuario': usuario})
    if send_cookie:
        response.set_cookie('usuario', usuario.id_usuario, expires=timezone.now() + timezone.timedelta(days=365))
    return response

def camara_imagen(request, id_camara):
    camara = get_object_or_404(Camara, id_camara=id_camara)
    return render(request, 'camara_imagen.html', {'camara': camara})
