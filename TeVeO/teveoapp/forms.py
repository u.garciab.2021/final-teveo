from django import forms
from .models import Usuario, Comentario

class UsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = 'nombre'

class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = 'comentario'
