from django.db import models

class Camara(models.Model):
    id_camara = models.CharField(max_length=150)
    name = models.CharField(max_length=150)
    latitud = models.CharField(max_length=150)
    longitud = models.CharField(max_length=150)
    localizacion = models.CharField(max_length=150)
    num_comentarios = models.IntegerField()
    enlace = models.CharField(max_length=200)
    numero_likes = models.IntegerField(default=0)
    usuarios_who_liked = models.CharField(max_length=200000, default='')

class Usuario(models.Model):
    nombre = models.CharField(max_length=150, default='Kim Dan')
    id_usuario = models.CharField(max_length=150)
    fuente_usuario = models.CharField(max_length=150, default='Arial')
    size_fuente = models.IntegerField(default=15)

class Comentario(models.Model):
    fecha = models.DateTimeField()
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    imagen = models.BinaryField()
    texto = models.TextField(max_length=300)
    id_camara = models.ForeignKey(Camara, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-fecha']



    # usamos el on delete para que si se borra una camara se borren los comentarios asociados a ella 
    # o si se borra un usuario se borren los comentarios asociados a el   
