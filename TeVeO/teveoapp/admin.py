from django.contrib import admin

from django.contrib import admin
from .models import Camara, Usuario, Comentario

# Register your models here.

admin.site.register(Camara)
admin.site.register(Usuario)
admin.site.register(Comentario)

