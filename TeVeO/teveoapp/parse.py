from xml.sax import ContentHandler
from .models import Camara

class Handler1(ContentHandler):
    def __init__(self):
        self.in_cameras = False
        self.in_content = False
        self.current_element = None
        self.current_camera = {}

    def startElement(self, name, attrs):
        if name == 'camaras':
            self.in_cameras = True
        elif name == 'camara':
            self.in_content = True
            self.current_camera = {'id': 'LIS1 - ', 'src': '', 'lugar': '', 'lat': '', 'lon': ''}
        elif name in ['id', 'src', 'lugar', 'coordenadas'] and self.in_cameras and self.in_content:
            self.current_element = name

    def characters(self, content):
        if self.current_element:
            if self.current_element == 'coordenadas':
                lat, lon = content.split(',')
                self.current_camera['lat'] = lat.strip()
                self.current_camera['lon'] = lon.strip()
            else:
                self.current_camera[self.current_element] += content.strip()

    def endElement(self, name):
        if name == 'camaras':
            self.in_cameras = False
        elif name == 'camara':
            self.in_content = False
            camera = Camara(
                id_camara=self.current_camera['id'],
                latitud=self.current_camera['lat'],
                longitud=self.current_camera['lon'],
                enlace=self.current_camera['src'],
                localizacion=self.current_camera['lugar'],
                num_comentarios=0
            )
            if not Camara.objects.filter(id_camara=self.current_camera['id']).exists():
                camera.save()
            self.current_camera = {}
        elif name in ['id', 'src', 'lugar', 'coordenadas']:
            self.current_element = None

class Handler2(ContentHandler):
    def __init__(self):
        self.in_cameras = False
        self.in_content = False
        self.current_element = None
        self.current_camera = {}

    def startElement(self, name, attrs):
        if name == 'list':
            self.in_cameras = True
        elif name == 'cam':
            self.in_content = True
            self.current_camera = {
                'id': 'LIS2 - ' + attrs.getValue('id'),
                'url': '',
                'place': '',
                'lat': '',
                'lon': ''
            }
        elif name in ['url', 'info', 'latitude', 'longitude'] and self.in_cameras and self.in_content:
            self.current_element = name

    def characters(self, content):
        if self.current_element:
            if self.current_element == 'url':
                self.current_camera['url'] += content.strip()
            elif self.current_element == 'info':
                self.current_camera['place'] += content.strip()
            elif self.current_element == 'latitude':
                self.current_camera['lat'] += content.strip()
            elif self.current_element == 'longitude':
                self.current_camera['lon'] += content.strip()

    def endElement(self, name):
        if name == 'list':
            self.in_cameras = False
        elif name == 'cam':
            self.in_content = False
            camera = Camara(
                id_camara=self.current_camera['id'],
                latitud=self.current_camera['lat'],
                longitud=self.current_camera['lon'],
                enlace=self.current_camera['url'],
                localizacion=self.current_camera['place'],
                num_comentarios=0
            )
            if not Camara.objects.filter(id_camara=self.current_camera['id']).exists():
                camera.save()
            self.current_camera = {}
        elif name in ['url', 'info', 'latitude', 'longitude']:
            self.current_element = None
